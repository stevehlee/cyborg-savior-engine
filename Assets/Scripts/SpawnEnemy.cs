﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {

	public GameObject enemy;
	public Transform spawn;/// remember that you must drag the enemy prefab onto this variable to define it

	
//	private float nextActionTime = 0.0f;
//	public float period = 0.1f;

	void Start()
	{
		Instantiate (enemy, spawn.transform.position, spawn.transform.rotation);
	}

	void Update()
	{
//		if (Time.time > nextActionTime ) {
//			nextActionTime += period;
//			Instantiate (enemy, spawn.transform.position, spawn.transform.rotation);
//		}
	}
}
