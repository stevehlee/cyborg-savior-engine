﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {
	
	public GameObject target;
	public float attackTimer;
	public float coolDown;
	public int damage = -10;
	public float MaxDistance = 1.5f;

	private float Distance;
	private RaycastHit hit;

	private Animator anim;

	// Use this for initialization
	void Start () {
		attackTimer = 0;
		coolDown = 0.5f;
		anim = gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		if (attackTimer > 0)
			attackTimer -= Time.deltaTime;
		if (attackTimer < 0)
			attackTimer = 0;

		if (Input.GetKeyUp(KeyCode.U) && attackTimer == 0)
		{
			anim.SetTrigger("Punch");
			Attack();
			attackTimer = coolDown;
		}
	}

	private void Attack()
	{
//		if (Physics.Raycast (transform.position, transform.TransformDirection(Vector3.right), out hit))
//		{
//			Distance = hit.distance;
//			Debug.Log(Distance);
//			if (Distance < MaxDistance)
//				hit.transform.SendMessage("AdjustCurrentHealth", damage, SendMessageOptions.DontRequireReceiver);
//		}
		BroadcastMessage ("punchAttack", SendMessageOptions.RequireReceiver);

	}
	

//	void OnTriggerEnter(Collider collider)
//	{
//		if (collider.gameObject.tag == "Enemy")
//		{
//			collider.gameObject.SendMessage("AdjustCurrentHealth", -10, SendMessageOptions.DontRequireReceiver);
//			//Destroy (collider.gameObject);
//			//Destroy (gameObject);
//		}
//	}
}
