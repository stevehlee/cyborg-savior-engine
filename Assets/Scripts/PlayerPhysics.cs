﻿//Objects tagged with whatever is in collisionMask is what applies this collisino property

/// Carl Lee
/// Version 1.0
/// 
/// -ChangeLog-
/// 
/// 3/5/2014 - Version 1.0
/// 	Creation

using UnityEngine;
using System.Collections;

[RequireComponent (typeof(BoxCollider))]
public class PlayerPhysics : MonoBehaviour {
	
	public LayerMask collisionMask;//What object to collide with
	
	private BoxCollider collider; //3D Box collider, since I can't get 2D raycast working
	private Vector3 s;
	private Vector3 c;
	
	private float skin = .005f;
	
	[HideInInspector]
	public bool grounded; //Grounded State
	[HideInInspector]
	public bool hitCeiling; //Ceiling state
	[HideInInspector]
	public bool movementStopped; //Left and Right Collision States, "Wall" states
	
	Ray ray;
	RaycastHit hit;
	
	void Start() {
		collider = GetComponent<BoxCollider>();
		s = collider.size;
		c = collider.center;
	}

	//Takes a movement vector and moves them by that amount
	//Will handle collision and resolution
	public void Move(Vector2 moveAmount) {
		
		float deltaY = moveAmount.y;
		float deltaX = moveAmount.x;
		Vector2 p = transform.position;

		// Check collisions left and right
		
		movementStopped = false;
		
		for (int i = 0; i<3; i ++) {
			float dir = Mathf.Sign(deltaX);
			float x = p.x + c.x + s.x/2 * dir; // Left, centre and then rightmost point of collider
			float y = (p.y + c.y - s.y/2) + s.y/2 * i; // Bottom of collider
			
			ray = new Ray(new Vector2(x,y), new Vector2(dir,0));
			Debug.DrawRay(ray.origin,ray.direction);
			if (Physics.Raycast(ray,out hit,Mathf.Abs(deltaX) + skin,collisionMask)) {
				// Get Distance between player and ground
				float dst = Vector3.Distance (ray.origin, hit.point);
				
				// Stop player's downwards movement after coming within skin width of a collider
				if (dst > skin) {
					deltaX = dst * dir - skin * dir;
				}
				else {
					deltaX = 0;
				}
				
				movementStopped = true;
				
				break;
				
			}
		}


		// Check collisions above and below
		grounded = false;
		hitCeiling = false;
		
		for (int i = 0; i<3; i ++) {
			float dir = Mathf.Sign(deltaY);
			float x = (p.x + c.x - s.x/2) + s.x/2 * i; // Left, centre and then rightmost point of collider
			float y = p.y + c.y + s.y/2 * dir; // Bottom of collider
			
			ray = new Ray(new Vector2(x,y), new Vector2(0,dir));
			Debug.DrawRay(ray.origin,ray.direction);
			//Collision detected
			if (Physics.Raycast(ray,out hit,Mathf.Abs(deltaY) + skin,collisionMask)) {
				// Get Distance between player and ground
				float dst = Vector3.Distance (ray.origin, hit.point);
				
				// Stop player's downwards movement after coming within skin width of a collider
				if (dst > skin) {
					deltaY = dst * dir - skin * dir;
				}
				else {
					deltaY = 0;
				}

				//It will ONLY be grounded if velocity is negitive, dir is negative
				if (dir < 0) {
					grounded = true;
				} else { //He collided with the ceiling
					hitCeiling = true;
				}

				break;
				
			}
		}

		//Fixes diagonal detection for raycsting
		//One extra ray will be drawn showing player direction
		if (!grounded && !movementStopped) {

			Vector3 playerDir = new Vector3 (deltaX, deltaY, 0);
			Vector3 o = new Vector3 (p.x + c.x + s.x/2 * Mathf.Sign (deltaX), p.y + c.y + s.y/2 * Mathf.Sign (deltaY), 0);
			ray = new Ray (o, playerDir.normalized);

			if (Physics.Raycast (ray, Mathf.Sqrt (deltaX * deltaX + deltaY * deltaY), collisionMask)) {
				grounded = true;
				deltaY = 0;
			}
		}

		Vector2 finalTransform = new Vector2(deltaX,deltaY);

		//Move character
		transform.Translate(finalTransform);
	}
	
}