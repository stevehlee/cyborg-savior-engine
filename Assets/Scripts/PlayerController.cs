//When you import the sprite, just make sure everything is set to 1 scale
//Don't worry about the cubes for now, the colliders should fix itself when the sprites are rectangle

/// Carl Lee
/// Version 1.0
/// 
/// -ChangeLog-
/// 
/// 3/5/2014 - Version 1.0
/// 	Creation

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerPhysics))]
public class PlayerController : MonoBehaviour {

	//Player handling
	public float speed; //Max Ground speed
	public float accel; //Ground acceleration
	public float gravity; //Gravity
	public float terminalSpeed; //Max fall speed
	public float fastTerminalSpeed; //Fast falling
	public float jumpHeight; //Jump force
	public int numAirJumps; //Number of Air Jumps
	public float dashSpeed; //How Fast
	public float dashTime; //For how long, in seconds

	private float direction; //Facing direction, left or right
	private float dashTimer; //Controls dashTime
	private float directionLock; //Holds which ever position 'foward' is during dash state, -1 is left, 0 or 1 is right
	private int airJumpsMade; //starts at 0, increments to numAirJumps
	private float currentSpeed; 
	private float targetSpeed; //Its just 1 * speed or 0 * speed, so its just speed
	private Vector2 amountToMove;

	private int leftDirFlag = 0;
	private int rightDirFlag = 0;

	public Animator anim;

	//private float upOrDown; //Checks for up or down input

	//States
	private bool fastFalling;
	private bool dashing;

	private PlayerPhysics playerPhysics; //Holds collision states, handles physics, etc...

	// Use this for initialization, cached for performance
	void Start () {
		playerPhysics = GetComponent<PlayerPhysics>();
		anim = gameObject.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		//Done before Input, Left and Right collision resolution
		//Done before so it doesn't block currentSpeed
		//Which makes you stick to walls...it's silly
		if (playerPhysics.movementStopped) {
			targetSpeed = 0;
			currentSpeed = 0;
		}

		targetSpeed = Input.GetAxisRaw ("Horizontal") * speed; //Input.GetAxisRaw returns 0 or 1 or -1, no in-between floats and no sensitivity
		if (Input.GetAxisRaw ("Horizontal") == -1) {
			rightDirFlag =0;
			direction = -1;
			if(leftDirFlag < 1){
				Debug.Log ("Currently Walking left | " +  + Time.deltaTime);
				//anim.SetFloat("direction" , direction + 1.0f); // -1 +1 =0
				transform.localScale = new Vector3(-1f,1f,1f); 
				leftDirFlag++;
			}


		} else if (Input.GetAxisRaw ("Horizontal") == 1) {
		    leftDirFlag =0 ;
			direction = 1;
			if(rightDirFlag < 1){
				Debug.Log ("Currently Walking Right | " +  + Time.deltaTime);
				//anim.SetFloat("direction" , direction); // -1 
				transform.localScale = new Vector3(1f,1f,1f);; 
				rightDirFlag++;
			}

		}

		//upOrDown = Input.GetAxisRaw ("Vertical"); //Again, -1 for down, 0 for normal, 1 for up
		currentSpeed = IncrementTowards (currentSpeed, targetSpeed, accel);
		anim.SetFloat ("Speed",currentSpeed);

		//Grounded
		if (playerPhysics.grounded) {
			//Refresh params
			amountToMove.y = 0;
			airJumpsMade = 0;
			fastFalling = false;

			//Jump
			if (Input.GetButtonDown("Jump")) {
				amountToMove.y = jumpHeight;
				anim.SetTrigger("Jump");
			}

			//Ground Dash
			if (Input.GetButtonDown("Dash")) {
				//If not already dashing
				if (!dashing) {
					dashing = true;
					directionLock = direction;
					dashTimer = Time.time + dashTime;
				}
			}

			//Dash loop
			if (dashing) {
				//If I still have time in dash state, keep up the speed
				if (Time.time < dashTimer) {
					amountToMove.x = dashSpeed * directionLock;
				} else { //Otherwise, I'm done with dash state
					dashing = false;
				}
			}
		}

		//Airborne
		if (!playerPhysics.grounded) {

			//On ceiling
			if (playerPhysics.hitCeiling) {
				amountToMove.y = 0; //Stops y momentum
			}

			//Air Jump
			if (Input.GetButtonDown ("Jump")) {
				if (airJumpsMade < numAirJumps) {
					amountToMove.y = jumpHeight;
					airJumpsMade++;
					//Puts you back into normal Airborne state
					fastFalling = false;
					dashing = false;
				}
			}

			//Air Dash
			if (Input.GetButtonDown ("Dash")) {
				//If not already dashing
				if (!dashing && airJumpsMade < numAirJumps) {
					dashing = true;
					directionLock = direction;
					dashTimer = Time.time + dashTime;
					airJumpsMade++;
					fastFalling = false;
				}
			}

			//Air Dash Loop
			if (dashing) {
				if (Time.time < dashTimer) {
					amountToMove.x = dashSpeed * directionLock;
					amountToMove.y = 0; //Not affected by gravity
				} else {
					dashing = false;
				}
			}

			//Enable Fast Fall
			if (Input.GetButtonDown ("Down")) {
				fastFalling = true;
			}

			//Caps fall speed for terminal velocity
			if (fastFalling) {
				amountToMove.y = -fastTerminalSpeed;
			}
			else if (amountToMove.y < -terminalSpeed) {
				amountToMove.y = -terminalSpeed;
			}

		}

		//Movement
		if (!dashing) {
			amountToMove.x = currentSpeed;
			amountToMove.y -= gravity * Time.deltaTime;
		}

		//Move player
		playerPhysics.Move (amountToMove * Time.deltaTime);

	}

	//Increase n towards target by acceleration
	private float IncrementTowards(float n, float target, float a) {
		if (n == target) {
			return n;
		}
		else {
			float dir = Mathf.Sign(target - n); //must n be increased or decreased, handles direction
			n += a * Time.deltaTime * dir;
			return (dir == Mathf.Sign(target-n)) ? n : target; //If n has now passed target speed, then return target, otherwise return n
		}
	}
}
