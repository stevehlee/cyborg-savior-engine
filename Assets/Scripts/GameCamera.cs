﻿//For now, Camera is set to be an orthographic 5
//If we want 1:1 pixel representation from photoshop, then use size 3.84
//Otherwise, it is (Screen Height/2/100)
//Ex: 768/2/100 = 384/100 = 3.84

//Another formula we can use is Screen Height/Tile Size/2
//Ex: 768/64/2 = 12/2 = 6

//Both these camera sizes work


/// Carl Lee
/// Version 1.0
/// 
/// -ChangeLog-
/// 
/// 3/5/2014 - Version 1.0
/// 	Creation

using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {

	public Transform target;
	public float trackSpeed;

	public void SetTarget(Transform t) {
		target = t;
	}

	void LateUpdate() {
		//Camera Tracking algorithm
		//Don't touch just yet, we should decide how the camera focuses on the player
		if (target) { //Setup this way so when player dies, camera doesn't freak out
			float x = IncrementTowards (transform.position.x, target.position.x, trackSpeed);
			float y = IncrementTowards (transform.position.y, target.position.y, trackSpeed);
			transform.position = new Vector3(x, y, transform.position.z);
		}
	}

	//Increase n towards target by acceleration
	//Taken from PlayerController
	private float IncrementTowards(float n, float target, float a) {
		if (n == target) {
			return n;
		}
		else {
			float dir = Mathf.Sign(target - n); //must n be increased or decreased, handles direction
			n += a * Time.deltaTime * dir;
			return (dir == Mathf.Sign(target-n)) ? n : target; //If n has now passed target speed, then return target, otherwise return n
		}
	}
}
