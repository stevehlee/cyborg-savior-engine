﻿/// Carl Lee
/// Version 1.0
/// 
/// -ChangeLog-
/// 
/// 3/5/2014 - Version 1.0
/// 	Creation

using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public GameObject player;
	public GameObject enemy;
	private GameCamera cam;

	// Use this for initialization
	void Start () {
		cam = GetComponent<GameCamera> ();
		SpawnPlayer ();
		SpawnEnemy ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SpawnPlayer() {
		cam.SetTarget((Instantiate (player,Vector3.zero, Quaternion.identity) as GameObject).transform);
	}

	void SpawnEnemy()
	{
		Instantiate (enemy, new Vector3(2.5f, 1f, 0f), Quaternion.identity);
	}
}
