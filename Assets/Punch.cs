﻿using UnityEngine;
using System.Collections;

public class Punch : MonoBehaviour {


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

	void punchAttack()
	{
		StartCoroutine (wait ());

	}

	IEnumerator wait()
	{
		collider.enabled = true;
		collider.isTrigger = true;
		yield return new WaitForSeconds (0.2f);
		collider.enabled = false;
		collider.isTrigger = false;
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			Debug.Log("Hit");
			other.gameObject.SendMessageUpwards("AdjustCurrentHealth", -10, SendMessageOptions.RequireReceiver);
		}
	}
}
